package com.rstech.topelancer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableJpaRepositories(basePackages = {"com.rstech"})
@ComponentScan(basePackages = {"com.rstech"})
@EntityScan(basePackages = {"com.rstech"})
public class TopelancerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TopelancerApplication.class, args);
	}

}
