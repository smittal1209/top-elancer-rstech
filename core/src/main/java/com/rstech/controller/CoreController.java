package com.topelancerauth.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author mj07yadav
 * @Desc controller for fetching data related to users 
 *
 */

@RestController
@RequestMapping("/core")
public class CoreController {




	@GetMapping("/test")
	public ResponseEntity<?> getCurrentUser() throws Exception{
		
		try{
			return ResponseEntity.ok(null);
		}catch(Exception e) {
			throw new Exception();
		}
		
	}
}
