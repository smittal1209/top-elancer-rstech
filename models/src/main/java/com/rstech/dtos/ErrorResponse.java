/**
 *@author 
 *@version  
 *@dateofcreation 
 * 
 */
package com.rstech.dtos;

import org.springframework.http.HttpStatus;

/**
 * This Error Dto is created for giving Standard Format to Error Responses
 * @author mj07yadav
 *
 */
public class ErrorResponse {
	
	private HttpStatus status;
	
	private Integer statusCode;
	
	private String errorMessage;
	
	public ErrorResponse() {
		
	}

	public ErrorResponse(HttpStatus status, Integer statusCode, String errorMessage) {
		super();
		this.status = status;
		this.statusCode = statusCode;
		this.errorMessage = errorMessage;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	


	

}
