package com.rstech.topelancerauth.exceptionHandlers;

import com.rstech.dtos.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

/**
 * This class acts as Global Exception Handler for all User related exceptions
 * @author mj07yadav
 *
 */
@ControllerAdvice
public class UserExceptionHandler extends ResponseEntityExceptionHandler {
	

	/**
	 * @Desc Handler for general Exceptions
	 * @author mj07yadav
	 * @param
	 * @return type
	 */
	@ExceptionHandler(Exception.class)
	@ResponseBody
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public ResponseEntity<?> generalException(HttpServletRequest request , Exception ex){
		System.out.println("inside global excetpion handler");
		
		ErrorResponse error = new ErrorResponse( HttpStatus.NOT_FOUND , HttpStatus.NOT_FOUND.value(),"Data not found");
		
		return buildResponseEntity(error);
	}
	
	 private ResponseEntity<Object> buildResponseEntity(ErrorResponse apiError) {
	       return new ResponseEntity<>(apiError, apiError.getStatus());
	  }
	
}
