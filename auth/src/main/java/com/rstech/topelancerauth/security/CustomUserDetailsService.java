package com.rstech.topelancerauth.security;



import com.rstech.entities.Roles;
import com.rstech.entities.User;
import com.rstech.entities.UserRoles;
import com.rstech.exception.ResourceNotFoundException;
import com.rstech.topelancerauth.repositories.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import com.rstech.topelancerauth.repositories.UserRepository;

import java.util.ArrayList;
import java.util.List;


/**
 * @changedBy mj07yadav
 * @changes  added list of roles which are fetched from database
 *
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserRoleRepository userRoleRepository;
    
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email)
            throws UsernameNotFoundException {
    	
        User user = userRepository.findByEmail(email)
                .orElseThrow(() ->
                        new UsernameNotFoundException("User not found with email : " + email)
        );
        
        List<Roles> roleList = new ArrayList<>();
        roleList = getUserRoles(user);
        
        return UserPrincipal.create(user,roleList);
    }

    @Transactional
    public UserDetails loadUserById(Long id) {
        User user = userRepository.findById(id).orElseThrow(
            () -> new ResourceNotFoundException("User", "id", id)
        );

        List<Roles> roleList = new ArrayList<>();
        roleList = getUserRoles(user);
        return UserPrincipal.create(user,roleList);
    }
    
    
    
    /**
     * This utility method is used for fetching roles by user User object 
     * 
     * @param user object
     * @return List of Roles .
     */
    public List<Roles> getUserRoles(User user){
    	
    	 List<UserRoles> userRoleList = userRoleRepository.findAllByUserId(user);
         List<Roles> roles = new ArrayList<>();
         
         if(!CollectionUtils.isEmpty(userRoleList)) {
      	   for(UserRoles userrole : userRoleList ) {
      		   roles.add(userrole.getRoleId());
      	   }
         }
         
         return roles;
    }
}