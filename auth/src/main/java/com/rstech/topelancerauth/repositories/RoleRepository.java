package com.rstech.topelancerauth.repositories;


import com.rstech.entities.Roles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


/**
 * @author mj07yadav
 *	@Desc Entity to getRoles from Role master table 
 */

@Repository
public interface RoleRepository extends CrudRepository<Roles, Integer>{

}
